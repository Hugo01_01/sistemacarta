package edu.ec.istdab.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named
@ApplicationScoped
public class CantonDTO {
private List<String> Canton;
	
	public CantonDTO() {
		String[] arreglo = {"Loja" , "Calvas", "Catamayo", "Celica", "Chaguarpamba", "Espindola", "Gonzanama",
				"Macara", "Olmedo", "Paltas", "Pindal", "Puyango", "Quilanga", "Saraguro","Sozoranga","Zapotillo" };
		this.Canton = new ArrayList<>(Arrays.asList(arreglo));
	}

	public List<String> getCanton() {
		return Canton;
	}
	
}
