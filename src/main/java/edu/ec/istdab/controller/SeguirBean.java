package edu.ec.istdab.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
//import javax.faces.view.ViewScoped;
import javax.inject.Inject;
//import javax.inject.Named;
import javax.inject.Named;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.PublicadorSeguidor;
import edu.ec.istdab.model.Usuario;
import edu.ec.istdab.service.IPersonaService;
import edu.ec.istdab.service.ISeguidorService;
//import edu.ec.istdab.service.ISeguidorService;
import edu.ec.istdab.util.MensajeManager;

@Named
@ViewScoped
public class SeguirBean implements Serializable {
	
	@Inject
	private IPersonaService personaService;
	
	@Inject
	private ISeguidorService seguidorService;
	
	@Inject
	private MensajeManager mensajeManager;
	
	private List<Persona> personas;
	
	private List<PublicadorSeguidor> seguidos;
	
	private Usuario us;
	
	@PostConstruct
	public void init() {
		FacesContext context = FacesContext.getCurrentInstance();
		this.us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
		this.listarSeguidos();
		this.listarPersonas();
	}
	
	public void listarSeguidos() {
		try {
			this.seguidos = this.seguidorService.listarSeguidos(this.us.getPersona());
		} catch (Exception e) {
			
		}
	}
	
	public void listarPersonas() {
		try {
			
			this.personas = this.personaService.listar();
			this.personas.remove(this.us.getPersona());
			this.personas.forEach(p ->{
				this.seguidos.forEach(s ->{
					if (s.getPublicador().getIdPersona() == p.getIdPersona()) {
						p.setEsSeguido(true);
					}
				});
			});
		} catch (Exception e) {
			
		}
	}
	
	public void seguir(Persona aSeguir) {
		try {
			List<Persona> seguidores = new ArrayList<>();
			List<Persona> publicadores = new ArrayList<>();
			seguidores.add(this.us.getPersona());
			publicadores.add(aSeguir);
			this.seguidorService.registrarPublicadoresSeguidores(seguidores, publicadores);
			mensajeManager.mostrarMensaje("AVISO", "Ahora sigues a " + aSeguir.getNombres()+" !", "INFO");
		} catch (Exception e) {
			mensajeManager.mostrarMensaje("AVISO", "Error al seguir a" + aSeguir.getNombres()+" !", "ERROR");
		}finally {
			this.listarSeguidos();
			this.listarPersonas();
		}
		
	}
	
	public void dejar(Persona aDejar) {
		try {
			List<Persona> seguidores = new ArrayList<>();
			List<Persona> publicadores = new ArrayList<>();
			seguidores.add(this.us.getPersona());
			publicadores.add(aDejar);
			this.seguidorService.dejarSeguir(seguidores, publicadores);
			mensajeManager.mostrarMensaje("AVISO", "Dejaste de seguir a " + aDejar.getNombres()+" !", "INFO");
		} catch (Exception e) {
			// TODO: handle exception
			mensajeManager.mostrarMensaje("AVISO", "Error al dejar de seguir a" + aDejar.getNombres()+" !", "ERROR");
		}finally {
			this.listarSeguidos();
			this.listarPersonas();
		}
	}
	
	public List<Persona> getPersonas() {
		return personas;
	}
	
	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}
	

}
