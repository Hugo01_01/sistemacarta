package edu.ec.istdab.controller;

import java.io.Serializable;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.RowEditEvent;

import edu.ec.istdab.model.Rol;
import edu.ec.istdab.service.IRolService;

@Named
@RequestScoped
public class RolBean implements Serializable{
	@Inject
	private IRolService rolservice;
	
	private List<Rol> lista;
	
	public List<Rol> getLista(){
		return lista;
	}
	
	public void setLista(List<Rol>lista) {
		this.lista = lista;
		
	}
	
	//LENA LA LISTA
	@PostConstruct
	private void init() {
		this.llenartabla();
	}
	
	public void llenartabla() {
		try {
			this.lista = this.rolservice.listar();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	 public void onRowEdit(RowEditEvent event) {
		 try {
			 this.rolservice.modificar((Rol)event.getObject());
			 FacesMessage msg = new FacesMessage("Rol Editado", ((Rol) event.getObject()).getTipo());
		     FacesContext.getCurrentInstance().addMessage(null, msg);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	        
	    }


}
