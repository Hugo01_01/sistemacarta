package edu.ec.istdab.dao;

import javax.ejb.Local;
import java.util.List;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Usuario;

@Local
public interface IPersonaDAO extends ICRUD<Persona>{
	List<Persona> listar(Usuario per) throws Exception;
}
