package edu.ec.istdab.dao;

import java.util.List;

import javax.ejb.Local;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.PublicadorSeguidor;
import edu.ec.istdab.util.ReporteSeguidor;

@Local
public interface ISeguidorDAO {
	Integer registrarPublicadoresSeguidores(List<PublicadorSeguidor> publicadores_seguidores);
	List<PublicadorSeguidor> listarSeguidores(Persona per);
	Integer dejarSeguir(List<PublicadorSeguidor> publicadores_seguidores);
	List<PublicadorSeguidor> listarSeguidos(Persona per);
	//METODO PARA TRABAJAR CON REPORTES
	List<ReporteSeguidor> listarSeguidores();

}
