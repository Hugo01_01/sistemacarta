package edu.ec.istdab.service;

import java.util.List;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.PublicadorSeguidor;
import edu.ec.istdab.util.ReporteSeguidor;

public interface ISeguidorService {

	Integer registrarPublicadoresSeguidores(List<Persona> seguidores, List<Persona> publicadores);
	List<PublicadorSeguidor> listarSeguidores(Persona per);
	Integer dejarSeguir(List<Persona> seguidores, List<Persona> publicadores);
	List<PublicadorSeguidor> listarSeguidos(Persona per);
	List<ReporteSeguidor> listarSeguidores();

}
