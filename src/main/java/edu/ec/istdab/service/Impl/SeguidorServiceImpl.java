package edu.ec.istdab.service.Impl;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import edu.ec.istdab.dao.ISeguidorDAO;
import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.PublicadorSeguidor;
import edu.ec.istdab.service.ISeguidorService;
import edu.ec.istdab.util.ReporteSeguidor;

@Named
public class SeguidorServiceImpl implements ISeguidorService, Serializable{
	@EJB
	private ISeguidorDAO dao;

	@Override
	public Integer registrarPublicadoresSeguidores(List<Persona> seguidores, List<Persona> publicadores) {
		List<PublicadorSeguidor> publicadores_seguidores = new ArrayList<>();
		try {
			publicadores.forEach(p ->{
				seguidores.forEach(s ->{
					PublicadorSeguidor ps = new PublicadorSeguidor();
					ps.setPublicador(p);
					ps.setSeguidor(s);
					ps.setFecha(LocalDateTime.now());
					publicadores_seguidores.add(ps);
				});
			});
			dao.registrarPublicadoresSeguidores(publicadores_seguidores);
		} catch (Exception e) {
			
		}
		return 1;
	}

	@Override
	public List<PublicadorSeguidor> listarSeguidores(Persona per) {
		// TODO Auto-generated method stub
		return dao.listarSeguidores(per);
	}

	@Override
	public Integer dejarSeguir(List<Persona> seguidores, List<Persona> publicadores) {
		List<PublicadorSeguidor> publicadores_seguidores = new ArrayList<>();
		try {
			publicadores.forEach(p ->{
				seguidores.forEach(s ->{
					PublicadorSeguidor ps = new PublicadorSeguidor();
					ps.setPublicador(p);
					ps.setSeguidor(s);
					ps.setFecha(LocalDateTime.now());
					publicadores_seguidores.add(ps);
				});
			});
			dao.dejarSeguir(publicadores_seguidores);
		} catch (Exception e) {
			
		}
		return 1;
	}

	@Override
	public List<PublicadorSeguidor> listarSeguidos(Persona per) {
		// TODO Auto-generated method stub
		return dao.listarSeguidos(per);
		
	}

	@Override
	public List<ReporteSeguidor> listarSeguidores() {
		// TODO Auto-generated method stub
		return dao.listarSeguidores();
	}
	
	

}
