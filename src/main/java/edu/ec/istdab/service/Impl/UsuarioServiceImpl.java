package edu.ec.istdab.service.Impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import edu.ec.istdab.dao.IUsuarioDAO;
import edu.ec.istdab.model.Usuario;
import edu.ec.istdab.service.IUsuarioService;

@Named
public class UsuarioServiceImpl implements IUsuarioService, Serializable{
	
	@EJB
	private IUsuarioDAO dao;
	
	@Override
	public Usuario login(Usuario us) {
		Usuario usuario = null;
		String clave = us.getClave();
		String claveHash = dao.traerPassHasched(us.getUsuario());
		try {
			if(!claveHash.isEmpty()) {
			if(BCrypt.checkpw(clave, claveHash)) {
				return dao.leerPorNombreUsuario(us.getUsuario());
			}
			}
		}catch(Exception e) {
			throw e;
		}
		return null;
	}
	
	@Override
	public Integer registrar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return dao.modificar(t);
	}

	@Override
	public Integer eliminar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Usuario> listar() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usuario listarPorId(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


}
