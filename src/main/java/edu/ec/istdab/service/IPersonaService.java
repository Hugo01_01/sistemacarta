package edu.ec.istdab.service;

import java.util.List;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Usuario;

public interface IPersonaService extends IService<Persona>{
	List<Persona> listar(Usuario per) throws Exception;

}
